"use strict";

// Load environment configuration
require('dotenv').config();

// Includes
const Express = require('express')
const IsOnline = require('is-online')
const Log = require('./modules/Log')
const Provider = require('./modules/Provider')
const ProductReloadDB = require('./modules/Product').reloadDatabase
let HTTP = require('./modules/HTTP')

// Create application instance
let app = Express();

// Configure Twig template engine
app.set('views', './application/views')
app.set('view engine', 'twig')
require('twig').cache(process.env.NODE_ENV === 'production')

Log.info('Hello and welcome to Inovscan !');

// Product auto-fetching system initialisation
(async () => {
    // Init function
    let init = (forceDownload) => {
        // Force download of assets only if we have internet connection
        Provider.fetch(forceDownload).then(() => {
            Log.success('Products fetched from provider !')
            ProductReloadDB()

            // Refresh product database every process.env.PRODUCT_REFRESH minutes
            setInterval(() => {
                Provider.fetch(false).then(() => {
                    Log.success('Products fetched from provider !')
                    ProductReloadDB()
                })
                // Error while refreshing
                .catch(err => Log.error('Failed to fetch product from provider: ' + err.message))
            }, process.env.PRODUCT_REFRESH * 60000)
        })
        // Error while fetching
        .catch((err) => {
            Log.error('Failed to fetch product from provider: ' + err.message + '\nRe-run in 1 minute...')
            // Schedules one-time init re-run
            setTimeout(init, 60000, false)
        })
    }

    // Check internet connection and then run init
    init(await IsOnline({
        timeout: 1500,
        version: 'v4'
    }))
})().then(() => {
    Log.success('Product auto-fetching system initialised !')
})
// Error handling
.catch((err) => {
    Log.error('Product auto-fetching system initialisation failed: ' + err.message)
})

// Initialize HTTP server
HTTP.initialize(app);

// Start listening
HTTP.listen(app);

// Handle SIGINT (Ctrl+C)
process.on('SIGINT', () => {
    process.exit();
});
