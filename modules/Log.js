"use strict";

// Includes
require('colors')
const Util = require('util')
const FSIO = require('./FSIO')

// Log channels
let channels = {
    debug: 'DEBUG'.grey,
    success: 'SUCCESS'.green,
    info: 'INFO'.cyan,
    warn: 'WARN'.yellow,
    error: 'ERROR'.red
}

// Log file stream
let logOut = FSIO.openLogStream('stdout.log')
let logErr = FSIO.openLogStream('stderr.log')

class Log {
    // Return current timestamp in local format
    static getTimestamp() {
        return '[' + new Date().toLocaleString("fr-FR", {hour12: false}).grey + ']'
    }

    // Add entry to standard log
    static log(channel, value) {
        let logEntry = Log.getTimestamp() + '[' + channel + '] ' + ((typeof value === 'object') ? Util.inspect(value, {colors: true}) : value)

        // If console detected, print into
        if (console)
            console.log(logEntry);

        // Print to standard log file
        logOut.write(logEntry + '\n')
    }

    // Add entry to error log
    static error(channel, value) {
        let logEntry = Log.getTimestamp() + '[' + channel + '] ' + ((typeof value === 'object') ? Util.inspect(value, {colors: true}) : value)

        // If console detected, print into
        if (console)
            console.log(logEntry);

        // Print to standard log file
        logErr.write(logEntry + '\n')
    }
}


module.exports = {
    debug: Log.log.bind(null, channels.debug),
    success: Log.log.bind(null, channels.success),
    info: Log.log.bind(null, channels.info),
    warn: Log.error.bind(null, channels.warn),
    error: Log.error.bind(null, channels.error)
};
