"use strict";

// Includes
const FS = require('fs')
const Path = require('path')
const RFS = require('rotating-file-stream')

class FSIO {
    // Return application root directory
    static getAppRoot() {
        return Path.resolve(__dirname + '/../')
    }

    static openLogStream(filename) {
        // Resolve absolute file path
        let filePath = FSIO.getAppRoot() + '/logs/' + filename

        // Test if file already exists
        if (FS.existsSync(filePath))
            FS.accessSync(filePath, FS.constants.W_OK)

        // Create rotating file stream
        return RFS((time, index) => {
                if (!index)
                    return filename

                return filename + '.' + index + '.gzip'
            },
            {
                path: FSIO.getAppRoot() + '/logs',
                size: process.env.LOG_SIZE_LIMIT,
                maxSize: process.env.LOG_ARCHIVE_SIZE_LIMIT,
                mode: 0o640,
                compress: 'gzip',
                history: FSIO.getAppRoot() + '/logs/' + filename + '.list'
            })
    }

    static readJSON(path) {
        let file = FS.readFileSync(FSIO.getAppRoot() + '/' + path, 'utf8');
        let json = [];
        try {
            json = JSON.parse(file);
            console.log("Products count: " + json.length);
        } catch (e) {
            console.log("not JSON");
        }
        return json;
    }
}

module.exports = FSIO
