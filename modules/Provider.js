"use strict";

const FS = require('fs')
const Del = require('del')
const Axios = require('axios')
const Download = require('download')
const Promisify =  require('util').promisify
const Log = require('./Log')
const FSIO = require('./FSIO')

// Assets directory
const ASSETS_DIR = FSIO.getAppRoot() + '/public/static/assets/'

class Provider {
    // Get all products from InovHub API
    static getProducts() {
        return Axios.get(process.env.PRODUCT_PROVIDER + '/products/get')
    }

    // Get one product from InovHub API
    static getProduct() {
        return Axios.get(process.env.PRODUCT_PROVIDER + '/products/get/' + barcode)
    }

    static async fetch(forceDownload) {
        // Fetch raw product list from API
        let rawProducts = await Provider.getProducts()

        // If download is forced, remove previously downloaded assets
        if (forceDownload)
            await Del([ASSETS_DIR + '**'])

        // Browse raw product list
        let products = []
        for (let product of rawProducts.data) {
            // Generate product and brand picture filename
            let pictureFilename = product.barcode + '.' + product.picture.replace(/^.+[\W]/, '')
            let brandFilename = product.brand_logo.substring(product.brand_logo.lastIndexOf('/') + 1)

            // Properly create product object from raw format...
            let formattedProduct = {
                barcode: product.barcode,
                name: product.name,
                description: product.description,
                img: 'static/assets/' + pictureFilename,
                brandLogo: 'static/assets/' + brandFilename,
                brand: product.brand
            }

            // ... and add it to refined product list
            products.push(formattedProduct)

            // If product picture file doesn't exists
            if (!FS.existsSync(ASSETS_DIR + pictureFilename)) {
                // Try to download product picture
                try {
                    await Download(product.picture, ASSETS_DIR, {filename: pictureFilename})
                } catch (err) {
                    products[products.indexOf(formattedProduct)].img = product.picture
                    Log.error('\'' + pictureFilename + '\' was not downloaded : ' + err.message + '. Product saved with remote image.')
                }

                // Download completed !
                Log.info('\'' + pictureFilename + '\' was successfuly downloaded.')
            }

            // If brand logo file doesn't exists
            if (!FS.existsSync(ASSETS_DIR + brandFilename)) {
                // Try to download brand logo
                try {
                    await Download(product.brand_logo, ASSETS_DIR, {filename: brandFilename})
                } catch (err) {
                    products[products.indexOf(formattedProduct)].brandLogo = product.brand_logo
                    Log.error('\'' + brandFilename + '\' was not downloaded : ' + err.message + '. Product\'s brand saved with remote image.')
                }

                // Download completed !
                Log.info('\'' + pictureFilename + '\' was successfuly downloaded.')
            }
        }

        Log.info(products.length + " products.")

        // Save refined product list to disk
        await Provider.saveToDisk(products, 'products.json')
    }

    // Save fetched data to disk
    static async saveToDisk(data, filename) {
        await Promisify(FS.writeFile)(FSIO.getAppRoot() + '/data/' + filename, JSON.stringify(data))
    }
}

module.exports = Provider
