"use strict";

// Includes
const FSIO = require('./FSIO')

let DB = [];

try {
    DB = require(FSIO.getAppRoot() + '/data/products.json')
} catch (err) {
    console.log("JSON inccorect");
}

class Product {
    static getProductInfo(barcode) {
        // Browse products database
        for (let i = 0; i < DB.length; ++i) {
            // Match product by barcode
            if (DB[i].barcode === barcode)
                return DB[i]
        }

        // No result
        return null
    }

    // Reload database from disk
    static reloadDatabase() {
        DB = FSIO.readJSON('data/products.json')
    }
}



module.exports = Product
