"use strict";

// Includes
const Express = require('express')
const Morgan = require('morgan')
const FSIO = require('./FSIO')
const Log = require('./Log')

class HTTP {
    // Initialize HTTP server and its binding with ExpressJS
    static initialize(app) {
        // Load logger
        app.use(Morgan(process.env.LOG_FORMAT, {
            stream: FSIO.openLogStream('access.log')
        }))

        // Load routes
        HTTP.loadRoutes(app)
    }

    // Open listening socket
    static listen(app) {
        app.listen(process.env.HOSTPORT, process.env.HOSTNAME, process.env.HOSTQUEUE, () => {
            Log.success('HTTP server started listening on http://' + process.env.HOSTNAME + ':' + process.env.HOSTPORT)
        })
    }

    // Initialise route middlewares
    static loadRoutes(app) {
        // Serve static assets
        app.use('/static', Express.static(FSIO.getAppRoot() + '/public/static'))

        // Load routes from Router module
        app.use(require('./Router'))

        // Handle HTTP 404 (Not found)
        app.use((req, res) => {
            res.status(404).render('error', {
                redirect: {
                    URI: '/',
                    timeout: 2000
                }
            })
        })

        // Handle other errors
        app.use((err, req, res, next) => {
            res.status(err.status || 500).render('error')
        })
    }
}

module.exports = HTTP
