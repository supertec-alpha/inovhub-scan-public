"use strict";

// Create router
let Router = require('express').Router()
let Product = require('./Product')

function nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

// Home page
Router.get('/', (req, res) => {
    res.render('index')
})

// Product page
Router.get('^/product/:product', (req, res) => {
    // Get product info from database
    let productInfo = Product.getProductInfo(req.params.product)

    // No info, 'no product' page
    if (productInfo === null) {
        res.render('noproduct', {
            redirect: {
                URI: '/',
                timeout: 5000
            }
        })
        return
    }

    res.render('product', {
        product: {
            img: productInfo.img,
            brandLogo: productInfo.brandLogo,
            name: productInfo.name,
            description: nl2br(productInfo.description)
        },
        redirect: {
            URI: '/',
            timeout: 10000
        }
    })
})

module.exports = Router
